default_target: pdf
.PHONY: default_target

LOOPS = 3

pdf: build/capitulo_1.pdf
.PHONY: pdf

html: build/capitulo_1.html
.PHONY: html

build:
	mkdir build

build/capitulo_1.pdf: | build
	cd cap1-resumen && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname capitulo_1 \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname capitulo_1 \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname capitulo_1 \
			-output-directory ../build \
			main.tex

build/capitulo_1.html: | build
	cd build && \
	pdf2htmlEX \
		--embed cfijo \
		--printing 0 \
		--optimize-text 1 \
		--fit-width 1200 \
		capitulo_1.pdf

clean:
	rm -r build
.PHONY: clean
