import sys

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import scipy as sp

n=7
G=nx.star_graph(n-1)

### relabel nodes######################
mapping=dict(zip(G.nodes(),range(0,n)))
G1=nx.relabel_nodes(G,mapping)
####centralidad_de_eigenvector#########
centr_eigen=np.ones((n))
A=nx.adjacency_matrix(G)
for t in range (0,201):
    centr_eigen=A*centr_eigen
centr_eigen=centr_eigen/np.linalg.norm(centr_eigen)
print(centr_eigen)
