import sys

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

n=7
G=nx.star_graph(n-1)

#####relabel nodes####################
mapping=dict(zip(G.nodes(),range(0,n)))
G1=nx.relabel_nodes(G,mapping)
#####centralidad de grado#############
centr_grad=np.zeros((n))
for vrtx in nx.nodes(G1):
   centr_grad[vrtx]=nx.degree(G1,vrtx)
print(centr_grad)

