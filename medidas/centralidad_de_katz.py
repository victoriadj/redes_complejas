import sys

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import scipy as sp

n=7
G=nx.star_graph(n-1)

### relabel nodes######################
mapping=dict(zip(G.nodes(),range(0,n)))
G1=nx.relabel_nodes(G,mapping)
####centralidad_de_eigenvector#########
alpha=0.5
betha=0.5
centr_katz=np.zeros((n))
aux=np.full((n),betha)
A=nx.adjacency_matrix(G)
for t in range (0,502):
    centr_katz=alpha*A*centr_katz+aux
centr_katz=centr_katz/np.linalg.norm(centr_katz)
print(centr_katz)
